# NSE script to run ssh-audit

[NSE](https://nmap.org/book/man-nse.html) script to run ssh-audit on SSH services using nmap.

# Dependencies

## ssh-audit

This makes an external call to [ssh-audit](https://github.com/jtesta/ssh-audit) so you must have that installed and available in your PATH already.

    pip install --user ssh-audit

## luarocks

Use [luarocks](https://luarocks.org/) to install a JSON library.

    luarocks --local install lunajson

Ensure NSE can find the library by setting the luarocks path in your environment.

    export LUA_PATH="$HOME/.luarocks/share/lua/5.4/?.lua;$HOME/.luarocks/share/lua/5.4/?/init.lua"
    export LUA_CPATH="$HOME/.luarocks/lib64/lua/5.4/?.so"

# Run

    nmap --script=./ssh-audit.nse <target>

# TODO

- Add script argument to filter recommendations based on critical/warning/recommended.
