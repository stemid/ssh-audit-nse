description = [[
Run ssh-audit.
]]

---
-- @usage
-- nmap --script=./ssh-audit.nse <target>
-- @output
-- Host script results:
-- PORT     STATE    SERVICE
-- 22/tcp   open     ssh
-- | ssh-audit:
-- |   recommendations:
-- |     critical:
-- |       del:
-- |         mac:
-- |
-- |             name: hmac-sha1
-- |             notes:
-- |
-- |             name: hmac-sha1-etm@openssh.com
-- |             notes:

author = "Stefan Midjich"
license = "Same as Nmap--See https://nmap.org/book/man-legal.html"
categories = {"discovery", "safe"}

portrule = function(host, port)
  return port.service == "ssh"
    and port.protocol == "tcp"
    and port.state == "open"
end

action = function(host, port)
  local stdnse = require "stdnse"
  local json = require "lunajson"
  local output = stdnse.output_table()
  local command = string.format("ssh-audit -j %s:%d", host.name, port.number)
  local handle = io.popen(command)
  local result = handle:read("*a")
  local decoded = json.decode(result)
  local recommendations = decoded.recommendations
  if decoded["recommendations"] ~= nil then
    output.recommendations = {}
    output.recommendations = recommendations
  end
  return output
end
